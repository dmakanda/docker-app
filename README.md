# WatchUsBuild-SimpleNodeAppWithDocker
Watch Us Build code for the Simple Node App With Docker screencast.

1. docker container run --name node-app --rm 8:8888 -v ~/Projets/startup/WatchUsBuild-SimpleNodeAppWithDocker/node/src:/usr/src/app/src node-app

2. docker container run --name node-db -p 9000:5432 --rm -v ~/Projets/startup/WatchUsBuild-SimpleNodeAppWithDocker/postgres/data:/var/lib/postgresql/data node-db:latest

3. docker container exec -it node-db psql -U postgres
