'use strict';

const express = require('express');
const app = express();
let bodyParser = require('body-parser');
let ejs = require('ejs');
let pg = require('pg');

let client = new pg.Client('postgres://postgres@172.17.0.1:9000/postgres');

let votes = {
  tartuffe: 0,
  grognon: 0
};

client.connect(function (err) {
  //if (err) throw err;
  client.query('SELECT number_of_votes FROM votes', function (err, result) {
    if (err) throw err;

    votes.tartuffe = result.rows[0].number_of_votes;
    votes.grognon = result.rows[1].number_of_votes;
  });
});

let urlencodedParser = bodyParser.urlencoded({ extended: false })

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views')

app.get('/', function (req, res) {
  res.render('pages/index', {
    votes: votes
  });
});

app.post('/vote', urlencodedParser, function(req, res) {
  let vote = req.body.yourVote;
  if(vote === 'tartuffe') {
    votes.tartuffe = votes.tartuffe + 1;
      client.query('UPDATE votes SET number_of_votes=\'' + votes.tartuffe + '\' WHERE option_name=\'tartuffe\'', function (err, result) {
        if (err) throw err;
      });
  } else if(vote === 'grognon') {
    votes.grognon = votes.grognon + 1;
      client.query('UPDATE votes SET number_of_votes=\'' + votes.grognon + '\' WHERE option_name=\'grognon\'', function (err, result) {
        if (err) throw err;
      });
  } else {
    console.log('Something went wrong: vote contains ' + vote);
  }
  res.redirect('/');
});

const PORT = 8888;
app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
